// 判断是否为控对象
export function isEmptyObject (obj) {
  for (var name in obj) {
    return false
  }
  return true
}

/*
* 深拷贝
* */
export function deepCopyObject (param) { // object 深拷贝
  const paramStr = JSON.stringify(param) // 序列化对象
  const newParam = JSON.parse(paramStr) // 还原
  return newParam
}

/*
* 格式化为时分秒 hh:mm:ss
* s 秒数
* */
export function formatSeconds (s) {
  let t = ''
  if (s > -1) {
    var hour = Math.floor(s / 3600)
    var min = Math.floor(s / 60) % 60
    var sec = s % 60
    if (hour < 10) {
      t = '0' + hour + ':'
    } else {
      t = hour + ':'
    }
    if (min < 10) {
      t += '0'
    }
    t += min + ':'
    if (sec < 10) {
      t += '0'
    }
    t += sec.toFixed(0)
  }
  return t
}

/**
 * 排序函数
 */
export function sortData (prop, reverse) { // 数据排序
  return function (obj1, obj2) {
    let val1 = reverse ? obj2[prop] : obj1[prop]
    let val2 = reverse ? obj1[prop] : obj2[prop]
    if (!isNaN(Number(val1)) && !isNaN(Number(val2))) {
      val1 = Number(val1)
      val2 = Number(val2)
    }
    if (val1 < val2) {
      return -1
    } else if (val1 > val2) {
      return 1
    } else {
      return 0
    }
  }
}
// 判断两个对象是不是相等
export function diff (obj1, obj2) {
  var o1 = obj1 instanceof Object
  var o2 = obj2 instanceof Object
  if (!o1 || !o2) { /*  判断不是对象  */
    return obj1 === obj2
  }

  if (Object.keys(obj1).length !== Object.keys(obj2).length) {
    return false
    // Object.keys() 返回一个由对象的自身可枚举属性(key值)组成的数组,例如：数组返回下表：let arr = ["a", "b", "c"];console.log(Object.keys(arr))->0,1,2;
  }
  for (var attr in obj1) {
    var t1 = obj1[attr] instanceof Object
    var t2 = obj2[attr] instanceof Object
    if (t1 && t2) {
      return this.diff(obj1[attr], obj2[attr])
    } else if (obj1[attr] !== obj2[attr]) {
      return false
    }
  }
  return true
}
