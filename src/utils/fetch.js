import axios from 'axios'
// import store from '@/store'
// import router from '@/router'
// 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api的base_url
  timeout: 50000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
  // Do something before request is sentconfig.headers['Content-Type'] = 'application/json';
  config.headers['Content-Type'] = 'application/json'
  // if (localStorage.getItem('access_token') && config.url.indexOf('/login') < 0) {
  //   config.headers.Authorization = 'Bearer ' + localStorage.getItem('access_token')
  // }
  return config
}, error => {
  // Do something with request error
  Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
  response => {
    if (response.data.code === 401 || response.data.code === 403) {
      // 登出
      // store.dispatch('FedLogout').then(() => {
      //   router.push({ path: '/login' })
      // })
    }
    return response.data
  },
  error => {
    if (error.isAxiosError) {
      return {
        msg: '网络异常：请检查网络后提交'
      }
    }
    if (error.response) {
      const code = error.response.status
      if ((code === 401 || code === 403)) {
        // 登出
        // store.dispatch('FedLogout').then(() => {
        //   router.push({ path: '/login' })
        // })
      }
    } else {
      return Promise.reject(error)
    }
  }
)

export default service
