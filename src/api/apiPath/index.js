import request from '../index'
const config = {
  // 获取用户菜单
  getAppRouters: {
    url: '/getAppRouters'
  },
  // 新增供应商
  appSupplierAddOrUd: {
    url: '/appSupplier/appSupplierAddOrUd',
    method: 'post'
  }
}

export default function req (funcName, data) {
  return request(config, funcName, data)
}
