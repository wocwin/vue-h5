import Vue from 'vue'
import FastClick from 'fastclick'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'
import 'vant/lib/index.css'
import '@/assets/styles/index.scss'
import PromiseHttp from '@/api/apiPath/index'
import { AMapBase } from '@/utils/AMapBase.js'
Vue.use(AMapBase)
Vue.use(Vant)

FastClick.prototype.onTouchEnd = function (event) {
  if (event.target.hasAttribute('type') && event.target.getAttribute('name') === 'userInfo') {
    return false
  }
}
FastClick.attach(document.body)
// 请求接口挂载在Vue原型上
Vue.prototype.$http = PromiseHttp

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
