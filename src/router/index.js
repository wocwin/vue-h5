import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home页面',
    meta: { title: 'Home页面' },
    component: Home
  },
  {
    path: '/testMap',
    name: '地图定位',
    meta: { title: '地图定位' },
    component: () => import('@/views/testMap.vue')
  },
  {
    path: '/task',
    name: '我的任务',
    meta: { title: '我的任务' },
    component: () => import('@/views/task')
  },
  {
    path: '/info',
    name: '我的消息',
    meta: { title: '我的消息' },
    component: () => import('@/views/info')
  },
  {
    path: '/personal',
    name: '个人中心',
    meta: { title: '个人中心' },
    component: () => import('@/views/personal')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
