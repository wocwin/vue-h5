const state = {
  // 导航菜单设置
  menuSettings: {}
}
const mutations = {
  SET_MENU_SETTINGS: (state, menuSettings) => {
    state.menuSettings = menuSettings
  }
}
const actions = {

}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
