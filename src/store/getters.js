const getters = {
  menuSettings: state => state.common.menuSettings,
  avatarUrl: state => state.user.avatarUrl,
  token: state => state.user.token,
  openId: state => state.user.openId,
  unionId: state => state.user.unionId,
  mpBinding: state => state.user.mpBinding, // 是否绑定微信公众号
  miniBinding: state => state.user.miniBinding, // 是否已经绑定微信小程序
  userInfo: state => state.user.userInfo,
  wxUserInfo: state => state.user.wxUserInfo,
  templateId: state => state.user.templateId, // 模板id
  subscribeData: state => state.user.subscribeData, // 订阅消息
  userType: state => state.user.userType // 当前登录用户 0-司机 1-供应商 2-废钢供应商
}
export default getters
