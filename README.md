# vue-h5

## 安装依赖

```
npm install
```

## 运行项目

```
npm run serve
```

## 分支说明

```
dev_vw   : 实现vw适配

dev_rem   : 实现rem适配
```

## 项目架构（使用技术栈）

### 1、vue H5 移动端项目 已经 实现 vw 适配/rem+vuex+axios 封装+vantUi

### 2、nav-bar 与 tab-bar 组件都已经封装

### 3、含有高德地图定位/标点并点击导航跳第三方 app 且实现导航路线（组件）——实例

## Git 提交规范

**格式示例：(一般 scope 可不写)**

```
<type>(<scope>): <subject>
```

### type

用于说明本次提交的类别，必须是以下可选类别中的一个:

- `ci`: ci 配置文件和脚本的变动;
- `chore`: 构建系统或辅助工具的变动;
- `polish`: 代码或者功能模块的改进和完善;
- `fix`: 代码 BUG 修复;
- `feat`: 新功能;
- `perf`: 性能优化和提升;
- `refactor`: 仅仅是代码变动，既不是修复 BUG 也不是引入新功能;
- `style`: 代码格式调整，可能是空格、分号、缩进等等;
- `docs`: 文档变动;
- `test`: 补充缺失的测试用例或者修正现有的测试用例;
- `revert`: 回滚操作;
