const path = require('path')
const resolve = dir => path.join(__dirname, dir)
const port = 7777
module.exports = {
  runtimeCompiler: true,
  outputDir: 'dist', // 构建时的输出目录
  // 配置 webpack-dev-server 行为。
  devServer: {
    open: true, // 编译后默认打开浏览器
    port: port, // 端口
    proxy: {
      '/dev-api': {
        target: process.env.VUE_APP_BASE_API,
        changeOrigin: true, // 是否跨域
        ws: true, // 是否支持websocket
        secure: false, // 如果是https接口，需要配置这个参数
        pathRewrite: {
          '^/dev-api': ''
        }
      }
    }
  },
  // 调整内部的 webpack 配置
  configureWebpack: () => {

  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('@assets', resolve('src/assets'))
      .set('@components', resolve('src/components'))
      .set('@views', resolve('src/views'))
      .set('@router', resolve('src/router'))
      .set('@store', resolve('src/store'))
  },
  css: {
    extract: true, // 是否使用css分离插件 ExtractTextPlugin
    sourceMap: false, // 开启 CSS source maps?
    loaderOptions: {
    }
  }

}
